set -e

trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT

usage="
Usage: $(basename $0) [OPTIONS]

Options:
  -b, --branch <branch>    The branch to deploy (must be dev, preprod or prod).
  -f, --folder <folder>	   The folder where config files are.
  -c, --contonly	   Only create a container. Exit before website deployement (useful at first run to create symfony skeleton for example).
  --no-install		   Do not install any package (useful when only updating).
  -h, --help		   This help.
"

contonly="false"
no_install="false"

TEMP=`getopt -o b:f:ch -l branch:,folder:,contonly,no-install,help -n $0 -- "$@"`
[ $? != 0 ] && echo "Terminating...\n" >&2 && exit 1
eval set -- "$TEMP" # quotes around `$TEMP': they are essential!

while true; do
    case "$1" in
	-b | --branch ) type="$2"; shift 2 ;;
	-f | --folder ) deploy_conf_folder="$2"; shift 2 ;;
	-c | --contonly ) contonly="true"; shift 2 ;;
	--no-install ) no_install="true"; shift 2 ;;
	-h | --help )
	    echo "$usage" && exit 0 ;;
	-- ) shift; break ;;
	* ) break ;;
    esac
done

if [[ $type != dev && $type != preprod && $type != prod ]]; then
    echo "You must specify a branch to deploy (dev, preprod or prod)." >&2
    exit 2
fi

if [ ! -d "$deploy_conf_folder" ]; then
    echo "$deploy_conf_folder is not a folder." >&2
    exit 2
fi

sourceFileTMP () { # tmp because we can use the function in functions.sh afterward.
    [[ ! -r $1 ]] && echo "$1 should exists or be readable." >&2 && exit 2
    . $1
}

sourceFileTMP "$deploy_conf_folder/$type/base.conf"
sourceFileTMP "$env_setter_path/check_dependances.sh"

if  [ ! -r "$deploy_conf_folder/$type/deploy-${type}.conf" ]; then
    say "Please rename $deploy_conf_folder/$type/deploy-${type}.conf.example to\
 $deploy_conf_folder/$type/deploy-${type}.conf, edit it and/or make the file readable.\n" i
    terminate 2
fi

if [[ $type != dev ]]; then
    read -p "Have you manually updated $deploy_conf_folder/$type/deploy-${type}.conf on the server ?\
 Please do it now if not. Press Enter when done." ans
fi

. $deploy_conf_folder/$type/deploy-${type}.conf

checkSetting_File "$deploy_conf_folder/deploy-common.sh" \
		  "deploy_conf_folder (variable) and deploy-common.sh (hardcoded)"
checkSetting_File $deploy_conf_folder "deploy_conf_folder"

. $env_setter_path/env_setter.sh -f $deploy_conf_folder
if [ "$containerType" = "web" ]; then
    . $env_setter_path/env_setter-web.sh
fi

if [ "$contonly" = "true" ]; then
    exit 0
fi

. $deploy_conf_folder/deploy-common.sh
sourceFile $deploy_conf_folder/deploy-${type}.sh

if [ "$containerType" = "web" ]; then
    say "\nRestarting nginx on the container..." l i
    $lxcexec systemctl restart nginx
    say "Done." l
fi
