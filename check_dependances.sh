if [ -z "$generic_bash_function" ] || [ ! -x "$generic_bash_function" ] ; then
    echo -e "Please install functions.sh script somewhere and/or set 'generic_bash_function' accordingly.\n"
    echo "To install it you can run :"
    echo "curl -s https://gitlab.com/Stylee/generic-bash-functions/-/raw/master/install.sh |bash"
    exit 1
fi

echo "Updating $generic_bash_function in host"
git -C "$(dirname "${generic_bash_function}")" pull
echo "done"
. "$generic_bash_function"

if [ -z "$env_setter_path" ] ||
       ! command -v $env_setter_path/env_setter.sh --help &> /dev/null ; then
    say "env_setter.sh script not found or not executable. Something's very wrong."
    say "(If you launched deploy-branch.sh, you should have env_setter.sh in the same folder."
    say "Is 'env_setter_path' correct ?\n" i
    say "To install env_setter files you can run :"
    say "curl -s https://gitlab.com/Stylee/env_setter/-/raw/master/install.sh |bash"
    terminate 1
fi

! command -v git &> /dev/null && say "Please install git first." >&2 && terminate 1

say "All dependances are installed. This is good.\n" i l 
