host="$1"
[ "$host" == "host" ] && commandsPrefix="" || commandsPrefix="$lxcexec"

nginxRepo="deb http://nginx.org/packages/debian $($commandsPrefix lsb_release -cs) nginx"
$commandsPrefix apt-cache policy| grep -F "$nginxRepo" -q && return 0

say "\nAdding nginx repository to the system and installing it..." l i
# https://nginx.org/en/linux_packages.html#Debian
nginxSourceFile="/etc/apt/sources.list.d/nginx.list"
urlRepoKey="https://nginx.org/keys/nginx_signing.key"
configRepo "$nginxSourceFile" "$nginxRepo" "nginx" "$urlRepoKey" '' "$host"

say "Please verify that the following nginx signing key fingerprint match to what is noted there :"
say "https://nginx.org/en/linux_packages.html#Debian\n" i
echo "###### VERIFY ME ######"
$commandsPrefix apt-key fingerprint ABF5BD827BD9BF62
say "###### VERIFY ME ######\n" i

temp_file=$($commandsPrefix mktemp)
read -p "Is the fingerprint matching y/N ? " a
if [ "$a" != y ]; then
    echo "Deleting the key..."								      
    $commandsPrefix apt-key del ABF5BD827BD9BF62
    echo "Deleting the repository..."
    $commandsPrefix grep -F -v "$nginxRepo" "$nginxSourceFile" > $temp_file && :
    $commandsPrefix bash -c "cat $temp_file > $nginxSourceFile"
    say "Please take appropriate action.\n Terminating...\n" i
    exit 2
fi

$commandsPrefix aptitude install -t $($commandsPrefix lsb_release -cs)-backports nginx
say "Done." l

