#!/bin/bash

function echoPhpV {
    $lxcexec php -v|head -n1|cut -c 5-7
}

setUpNginxSite() {
    local confFile=$1; local dest=$2; local subDir=$3; local host=$4; local domain=$5
    local shared="$6"; local siteDirName=$7

    checkFunctionArgs "$1" "$2" "$3" "$5"

    [ "$host" == "host" ] && local lxcexec=""
    pushConfigFile "$confFile" "$dest" "$subDir" "" "" "$host" "$shared"

    local f="/etc/nginx/sites-enabled"
    [ -z "$lxcexec" ] && [[ ! -e $f ]] && mkdir $f
    [ -n "$lxcexec" ] && $lxcexec [[ ! -e $f ]] && $lxcexec mkdir $f

    $lxcexec ln -s --force $dest$confFile $f

    $lxcexec /opt/rpl/rpl "server_name .+" "server_name $domain;" "$dest/$confFile"
    if [ $type == "dev" ]; then
	$lxcexec /opt/rpl/rpl "ssl_certificate .+" "ssl_certificate /etc/mkcert/${domain}.pem;" "$dest/$confFile"
	$lxcexec /opt/rpl/rpl "ssl_certificate_key .+" "ssl_certificate_key /etc/mkcert/${domain}-key.pem;" "$dest/$confFile"
    fi

    if [ "$host" != 'host' ]; then # Because configuring reverse proxy if host.
	$lxcexec /opt/rpl/rpl "fastcgi_pass .+" "fastcgi_pass unix:/var/run/php/php$(echoPhpV)-fpm.sock;" "$dest/$confFile"
    fi

    if [ -n "$siteDirName" ] && [ "$siteDirName" != "adminer" ] ; then
	[ "$siteTechno" == "wordpress" ] && local wwwSubfolder=""
	[ "$siteTechno" == "symfony" ] && local wwwSubfolder="public"
	$lxcexec /opt/rpl/rpl "^[ \t]+root .+" "    root /var/www/$siteDirName/$wwwSubfolder;" "$dest/$confFile"

    fi # In case it's 'adminer', correct value should be in the nginx ("shared") conf file.
}

installDevCert() {
    # https://computingforgeeks.com/how-to-create-locally-trusted-ssl-certificates-on-linux-and-macos-with-mkcert/
    # https://github.com/FiloSottile/mkcert

    say "\nConfiguring ssl certificates with mkcert, on the host..." i l
    if [ ! -e /usr/local/bin/mkcert ]; then
	apt install wget libnss3-tools
	export VER="v1.4.3"
	wget -O mkcert https://github.com/FiloSottile/mkcert/releases/download/${VER}/mkcert-${VER}-linux-amd64
	chmod +x mkcert
	mv mkcert /usr/local/bin
	## TODO modif /etc/sudoers
    fi

    if ! su $fileOwnerOnHost -c "sudo -l tee" || ! su $fileOwnerOnHost -c "sudo -l mkcert"; then
	mkcertSudoFileContent="$fileOwnerOnHost ALL=(ALL) NOPASSWD: /usr/local/bin/mkcert
$fileOwnerOnHost ALL=(ALL) NOPASSWD: /usr/bin/tee
$fileOwnerOnHost ALL=(ALL) NOPASSWD: /usr/local/share/ca-certificates/mkcert_development_CA_200068238944531372982093324632769013702.crt
$fileOwnerOnHost ALL=(ALL) NOPASSWD: /usr/sbin/update-ca-certificates"

	say "Some mkcert related commands need to be allowed to run with sudo, should I add\
 the following to /etc/sudoers.d/mkcert to enable them for user $fileOwnerOnHost ? Y/n "
	say "$mkcertSudoFileContent \n" i
	read ans
	[ "$ans" == Y ] || [ "$ans" == y ]  || [ "$ans" == "" ] && echo "$mkcertSudoFileContent" >> /etc/sudoers.d/mkcert
    fi


    pushd /home/$fileOwnerOnHost
    su -c "mkcert --install" $fileOwnerOnHost
    su -c "mkcert $*" $fileOwnerOnHost
    pushFile /home/$fileOwnerOnHost/${1}.pem /etc/mkcert/
    pushFile /home/$fileOwnerOnHost/${1}-key.pem /etc/mkcert/
    rm /home/$fileOwnerOnHost/${1}.pem /home/$fileOwnerOnHost/${1}-key.pem
    popd
    say "Done." l
}

function installProdCert() {
    # Last test took from https://www.cyberciti.biz/faq/find-check-tls-ssl-certificate-expiry-date-from-linux-unix/
    # Should return error code if cert does not exist or is expired.
    # Maybe not be the best way or may not be robust. I don't know.
    [[ $type != preprod && $type != prod ]] && return 0

    if $lxcexec [ -e /etc/ssl/certs/localhost.crt ] &&
	    $lxcexec openssl x509 -enddate -noout -in localhost.crt -checkend 0; then
	return 0
    fi

    say "\nConfiguring self-signed cert for secure local communication with proxy..." l i
    # https://www.humankode.com/ssl/create-a-selfsigned-certificate-for-nginx-in-5-minutes
    pushConfigFile "localhost.cert.conf" "/etc/nginx/" "${cont//-$type/}/nginx/" '' '' '' 'sharedConfFile'
    $lxcexec openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/localhost.key -out /etc/nginx/localhost.crt -config /etc/nginx/localhost.cert.conf
    $lxcexec mv /etc/nginx/localhost.crt /etc/ssl/certs/localhost.crt
    $lxcexec mv /etc/nginx/localhost.key /etc/ssl/private/localhost.key
    say "Done." l
}

function setupReverseProxy() {
    checkFunctionArgs "$1" "$2" "$3" "$4" "$5"
    local reverseProxyConf="$1"; local dest="$2"; local subDir="$3"; local FQDN="$4"
    local noCert="$6" ; local noRestart="$7"
    local allFQDN="$5"

    say "\nConfiguring nginx reverse proxy on the host..." l i
    ip="$(lxc list -f csv ^$cont$ -c 4|cut -f1 -d' ')"
    setUpNginxSite "$reverseProxyConf" "$dest" "$subDir" "host" "$FQDN"

    if [ "$noCert" != "noCert" ]; then
	# We probably don't need to push nginx.conf file if adding a reverse proxy w/o a cert.
	# I might be wrong !
	pushConfigFile "nginx.conf" "/etc/nginx/" "${cont//-$type/}/nginx/reverse-proxy/" '' '' 'host' 'sharedConfFile'

	/opt/rpl/rpl "proxy_pass https.+" "proxy_pass https://${ip}:443;" "/etc/nginx/sites-available/$reverseProxyConf"

	if ! certbot certificates 2> /dev/null|grep -q "Domains: $FQDN$"; then
	    /opt/rpl/rpl "ssl_certificate.+" "" "/etc/nginx/sites-available/$reverseProxyConf"
	    say "\nConfiguring ssl certificates (on the host, for the reverse proxy)..." i
	    aptUpdate "host"
	    apt install certbot python3-certbot-nginx
	    certbot --nginx -d "$allFQDN"
	fi
    fi

    if [ "$noRestart" != "norestart" ]; then
	say "\nRestarting nginx..." i && systemctl restart nginx
    fi
    say "Done." l
# certbot renew
}

setMaxUploadSize() {
    local size="$1" # 25M for example
    local number="${size//[!0-9]/}"
    local unit="${size//[!A-Z]/}"
    [ -z "$unit" ] && unit="${size//[!a-z]/}"
    local newsize;
    if [ ${#unit} -gt 1 ]; then
	say "Unit argument passed to setMaxUploadSize function must be 'K', 'M' or 'G'.\n" i
	terminate 2
    fi

    if [ -n "$unit" ] && [ "${size: -1}" != "$unit" ]; then
	say "Unit argument passed to setMaxUploadSize function must be 'K', 'M' or 'G' and be put at the end.\n" i
	terminate 2
    fi

    say "\n Setting maximum upload size to $size..." i
    $lxcexec /opt/rpl/rpl "^upload_max_filesize = .+" "upload_max_filesize = $size;" "/etc/php/$(echoPhpV)/fpm/php.ini"
    # need "/etc/php/$(echoPhpV)/cli/php.ini" too ? and cgi ?

    if [ -z "$unit" ]; then
	newsize=$(($number+40000000))
    else
	if [ $unit == "m" ] || [ $unit == "M" ]; then
	    newsize=$(($number+5))
	fi
	if [ $unit == "k" ] || [ $unit == "K" ]; then
	    newsize=$(($number+50))
	fi
	if [ $unit == "g" ] || [ $unit == "G" ]; then
	    newsize=$(($number+1))
	fi
    fi

    say "\n Setting maximum upload size to $newsize$unit..." i
    $lxcexec /opt/rpl/rpl "^post_max_size = .+" "post_max_size = $newsize$unit;" "/etc/php/$(echoPhpV)/fpm/php.ini"
    say "Done." l

    # (Reminder: Restarting nginx wasn't enough. Restarting the container made the modifications work.)
    # todo?: modify nginx conf too ?
}

function composerDumpEnv {
    checkFunctionArgs "$1"
    local dir="$1"

    if [[ $type == "preprod" || $type == "prod" ]]; then
	# https://symfony.com/doc/4.3/configuration.html#configuration-environments
	say "\nRunning composer dump-env to improve performance..." l i
	$lxcexec su - www-data -s /bin/bash -c "composer dump-env prod -d $dir"
	say "Done." l
    fi
}

function buildAssets {
    local siteDirName="$1"; local wwwDir="$2"

    say "\n Building assets for ${siteDirName} (composer, npm, webpack)..." l i

    composerBuilding "$siteDirName" "$wwwDir"
    npmBuilding "$siteDirName"
    webpackBuilding "$siteDirName"

    say "Done." l
}

function composerBuilding {
    say "Building composer stuff..."
    local siteDirName=$1; local wwwDir=$2
    local method="install"
    if [[ $type == dev ]]; then
	read -N1 -p "Press U to 'composer update' or anything else to 'composer install'" ans
	[ "$ans" == U ] && method="update"
    fi

    if $lxcexec [ ! -f "$wwwDir/composer.json" ] ; then
	say "No composer.json found in ${wwwDir}, skipping composer ${method}."
	return
    fi

    $lxcexec su - www-data -s /bin/bash -c "composer $method -d $wwwDir"
}

function detectPackageJsonFile() {
    local siteDirName="$1"
    [ "$type" == dev ] && local lxcexec=""
    $lxcexec find "$(returnDataFolder "$siteDirName")" -iname 'package.json' -not -path '*/node_modules/*' -not -path '*/vendor/*' -not -path '*/public/bundles/*'
}

function detectPackageJson {
    local what="$1"; local siteDirName="$2"
    local detectedFilesNumber; local detectedFiles

    detectedFiles=$(detectPackageJsonFile "$siteDirName")

    if [ -z "$detectedFiles" ];then
	read -t0.1 -p "No package.json detected anywhere, skipping $what."
	echo ''
	return
    fi

    detectedFilesNumber="$(detectPackageJsonFile "$siteDirName" | wc -l)"

    if [ $detectedFilesNumber -gt 1 ]; then
	# https://stackoverflow.com/a/39581815/9770656 How do I add a line break for read command?
	read -t1 -p "More than one package.json detected, $what aborted. You have to investigate."$'\n'
	read -t1 -p "Here's the detected files :"$'\n'
	read -t5 -p "$detectedFiles"$'\n'
	exit 2
    fi

    read -p "I'v detected the following package.json : $detectedFiles. Should I '$what' with this one ?
Y/n (n just skip this step) ? " ans
    [ "$ans" == Y ] || [ "$ans" == y ] || [ -z "$ans" ] && dirname "$detectedFiles" || echo ''
    return
}

function npmBuilding {
    say "Building npm stuff..."
    local siteDirName="$1"
    local method="update"
    [[ $type == preprod || $type == prod ]] && local method="install"

    npmFolder="$(detectPackageJson "npm $method" "$siteDirName")"

    [ -z "$npmFolder" ] && return

    if [ "$type" == dev ]; then
	local lxcexec=""
	local user="$fileOwnerOnHost"
	local flag=""
    fi

    if [[ $type == preprod || $type == prod ]]; then
	local user=webuser
	local flag="--production"
    fi

    $lxcexec su - $user -s /bin/bash -c "cd $npmFolder && npm $method $flag"
}

function webpackBuilding {
    say "Building webpack stuff..."
    npmFolder="$(detectPackageJson "webpack building" "$siteDirName")"
    [ -z "$npmFolder" ] && return

    if [ "$type" == dev ]; then
	finalAdvices+=( "You can now run 'npm run watch' (from root of project) for webpack to watch for changes and build assets." )
    fi

    if [[ $type == preprod || $type == prod ]]; then
	$lxcexec su - webuser -s /bin/bash -c "cd $npmFolder && npm run build"
    fi
}

function configEnvFiles {
    local DBname="$1"; wwwDir="$2"; domainVariableName="$3"; domain="$4"

    say "\nInstalling other configuration files and/or setting variables to correct values..." l i

    [ "$type" == dev ] && local lxcexec=""

    ! $lxcexec [ -f "$wwwDir/.env.local" ] && $lxcexec touch "$wwwDir/.env.local"

    #https://stackoverflow.com/questions/62412312/symfony-makemigration-the-metadata-storage-is-not-up-to-date-please-run-the
    kv="DATABASE_URL=mysql://$dbuser:$dbpassword@127.0.0.1:3306/${DBname}?serverVersion=mariadb-$(lxc exec $cont -- mariadb --version |cut -f6 -d ' '|cut -f1 -d'-')"
    commentAndOrAddKeyValue "$kv" "$wwwDir/.env.local"
    commentAndOrAddKeyValue "${domainVariableName}=$domain" "$wwwDir/.env"
    commentAndOrAddKeyValue "MJ_APIKEY_PRIVATE=$MJ_APIKEY_PRIVATE" "$wwwDir/.env.local"
}
