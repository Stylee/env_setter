if [ "$type" != "dev" ]; then
    return 0
fi

if [ "$1" == "" ]; then
    say "websitePath argument needed." >&2
    terminate 1
fi

websitePath="$1"
websitePathContent=$($lxcexec bash -c "ls -A $websitePath")
# composer won't create a new project if folder not empty, so testing for folder emptyness's good.
[ $contonly != "true" ] && [[ -n "$websitePathContent" ]] && return 0

read -p "Should I create a symfony skeleton (with twig and annotations) ? Y/n " ans
[ "$ans" != Y ] && [ "$ans" != y ] && [ "$ans" != "" ] && return 0
# $lxcexec bash -c "cd $websitePath; chown www-data:www-data $websitePath"
$lxcexec su www-data -s /bin/bash -c "cd $websitePath; composer create-project symfony/skeleton ."
$lxcexec su www-data -s /bin/bash -c "cd $websitePath; composer require annotations"
$lxcexec su www-data -s /bin/bash -c "cd $websitePath; composer require twig"
if [ "$DB_needed" = "yes" ]; then
    $lxcexec su www-data -s /bin/bash -c "cd $websitePath; composer require symfony/orm-pack"
    $lxcexec su www-data -s /bin/bash -c "cd $websitePath; composer require --dev symfony/maker-bundle"
fi
