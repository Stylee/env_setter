
addDeviceToCont() {
    # Should only be used when setting up a dev environnement.
    # For prod and preprod, data are fetched from a repo branch.
    #
    # devDir variable will be empty for prod and preprod.
    # I could use the `returnDataFolder` function instead, but it makes no
    # sense since addDeviceToCont should really be useful only when
    # setting up a dev environnement.

    say "\nAdding shared storage to container..." l i

    local name="$1"; local folder="$2"; local path="$3"
    if lxc config device list $cont | grep -q "^$name$"; then
	# removing in case things changed
	lxc config device remove $cont "$name"
    fi

    lxc config device add "$cont" "$name" disk source="${devDir}$folder" path="$path"
    say "Done." l
}

setLxcMapping() {
    # https://lxd.readthedocs.io/en/latest/userns-idmap/
    # https://superuser.com/questions/1174344/syntax-for-setting-lxd-container-raw-idmap
    say "\nMapping uid:guid of '$fileOwnerOnHost' to www-data..." l i
    echo -en "both $(id -u $fileOwnerOnHost) $($lxcexec id -u www-data)" | lxc config set $cont raw.idmap -
    lxc restart $cont
    say "Done." l
}

addProxyDevice() {
    # https://community.jitsi.org/t/jitsi-quick-install-and-lxd-in-ubuntu-18-04/19142/24
    # https://blog.simos.info/how-to-use-the-lxd-proxy-device-to-map-ports-between-the-host-and-the-containers/
    # https://discuss.linuxcontainers.org/t/difference-between-network-forward-and-proxy-device/13421/2
    # https://linuxcontainers.org/lxd/docs/master/instances/#type-proxy
    local port="$1"; local protocol="$2"

    say "\nAdding proxy device for port $port on $protocol protocol..." l i

    name="${cont}${port^^}${protocol}"
    if lxc config device list "$cont" | grep -q "^$name$"; then
	# removing in case things changed
	lxc config device remove "$cont" "$name"
    fi

    lxc config device add "$cont" "$name" proxy listen="${protocol}:0.0.0.0:$port" connect="${protocol}:127.0.0.1:$port"
    say "Done." l
}
