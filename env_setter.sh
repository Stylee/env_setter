#!/bin/bash
set -e
declare -A finalAdvices

trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT

usage="
Usage: $(basename $0) [OPTIONS]

Options:
  -f, --conf-folder <folder>    The full absolute path to the folder's conf files.
  -c, --conf-file <file>	The filename of the conf file used by this script to set up the main variables.
      		  		Must be in 'conf-folder'.
  -h, --help			This help.
"

TEMP=`getopt -o f:c:h -l conf-folder:,conf-file:,help -n $0 -- "$@"`
[ $? != 0 ] && echo "Terminating...\n" >&2 && exit 1
eval set -- "$TEMP" # quotes around `$TEMP': they are essential!

while true; do
    case "$1" in
	-f | --conf-folder ) confFolder="$2"; shift 2 ;;
	-c | --conf-file ) confFile="$2"; shift 2 ;;
	-h | --help )
	    echo "$usage" && exit 0 ;;
	-- ) shift; break ;;
	* ) break ;;
    esac
done

if [[ -z "$confFile" && ( -z "$cont" || -z "$generic_bash_function" ) ]]; then
    echo "You should probably provide a config file with correct variables set."
    echo "(Or use a specific deploy script)."
    exit 2
fi

if [[ -n $confFile && ( ! -e $confFile || ! -r $confFile ) ]]; then
    echo -e "The file or directory the '$confFile' variable(s) point to does not exist or is not readable.\n" i >&2
    exit 2
fi

[ -n "$confFile" ] && . "$confFile"

if [ -z "$generic_bash_function" ] || [ ! -x "$generic_bash_function" ] ; then
    echo -e "Please install functions.sh script somewhere and set 'generic_bash_function' accordingly.\n"

    echo "To install it you can run :"
    echo "curl -s https://gitlab.com/Stylee/generic-bash-functions/-/raw/master/install.sh |bash"
fi

[ ! -x "$(command -v say)" ] && . "$generic_bash_function"

checkSetting_File "$confFolder" "confFolder"

if [[ $type == "dev" ]]; then
    checkSetting_File "$devDir" "devDir"
    # [ ! "$(ls -A $devDir)" ] && echo "$devDir is empty." >&2 && terminate 1
fi

lxcexec="lxc exec $cont --"
lxdInitPreseedFile="lxd_init_preseed.yaml"
# https://askubuntu.com/questions/1066541/how-to-get-the-real-path-of-a-sh-file-that-is-being-sourced
envSetterDir="$(realpath "$(dirname "$BASH_SOURCE")")"

. $envSetterDir/env_setter-file-management.sh
. $envSetterDir/env_setter-container-management.sh

aptUpdate() {
    [ "$1" == 'host' ] && local lxcexec=""
    say "\nUpdating packages index... " l i nlb
    if { $lxcexec apt-get update 2>&1 || echo E: update failed; } | grep -q '^[WE]:'; then
	say "\nUpdating packages index failed. Maybe check your network connectivity ?" i >&2
	terminate 1
    fi
    say "Done.\n" l i
}

# todo : warn somehow that this script is for setting up a debian
# buster (dev) environnement in a lxc container from a bullseye/sid (11) host
# todo : verify if the hos...

if [ "$no_install" != "true" ] ; then
    aptUpdate "host" && apt-get install snapd

    if [[ $type == dev ]]; then
	apt-get install sudo
    fi

    say "\nInstalling lxd with snap..." l i
    snap install core #https://github.com/lxc/lxd/issues/7381
    snap install lxd
    say "Done.\n" l i
fi

####### Note for when I'll use this for production use.
# https://linuxcontainers.org/fr/lxd/getting-started-cli/
# For testing purposes you can create a loop-backed storage pool. But
# for PRODUCTION USE it is recommended to use an empty partition (or
# full disk) instead of loop-backed storages (Reasons include:
# loop-backed pools are slower and their size can't be reduced).

if [ $(lxc profile show default 2> /dev/null |wc -l) -le 5 ]; then
    say "Configuring lxd (lxd init)..."
    say "You can read https://linuxcontainers.org/fr/lxd/getting-started-cli/#initial-configuration\
 to help you answer the questions".
    # lxd init < ./$lxdInitPreseedFile
    lxd init
fi

m="Please upgrade to lxd version 4.7."
[ ! $(lxc --version |cut -d . -f 1) -ge 4 ] && echo "$m" >&2 && exit 1
m="Please upgrade to at least lxd version 4.7."
[ $(lxc --version |cut -d . -f 1) -eq 4 ] && [ $(lxc --version |cut -d . -f 2) -lt 7 ]\
    && echo "$m" >&2 && exit 1

if [ -z "$(lxc list -f csv ^$cont$)" ]; then
    say "Installing debian 11 in '$cont' lxc container." l
    lxc launch images:debian/11/amd64 $cont
    say "Done.\n" l i
fi
m="Starting '$cont' container..."
[ -z "$(lxc list -f csv ^$cont$ status=RUNNING)" ] && say "$m" && lxc start $cont

aptUpdate && $lxcexec apt-get upgrade

if [ "$no_install" != "true" ] ; then
    $lxcexec apt-get install aptitude
fi

if [ "$no_install" != "true" ] ; then
    say "\nConfiguring the system to use less space..." i l
    $lxcexec aptitude purge vim

    #https://unix.stackexchange.com/questions/345555/how-to-autoremove-packages-with-aptitude-package-manager
    $lxcexec aptitude -o Aptitude::Delete-Unused=1 install

    $lxcexec aptitude install localepurge
    d="/usr/share/locale"
    $lxcexec mkdir -p /temp_locale_deletion
    $lxcexec bash -c "cp -r $d/locale.alias $d/fr/ $d/en* /temp_locale_deletion/"
    $lxcexec bash -c "rm -rf $d/*"
    $lxcexec bash -c "cp -r /temp_locale_deletion/* $d/"
    $lxcexec rm -rf /temp_locale_deletion
    pushConfigFile "01_nodoc" "/etc/dpkg/dpkg.conf.d/" "common"
    $lxcexec apt autoremove
    say "Done." l

    say "\nInstalling some packages..." i l

    $lxcexec aptitude install curl gnupg2 ca-certificates lsb-release openssh-server git unzip python3-chardet python3-regex # python3-chardet and python3-regex is for rpl
    say "Done." l

    say "\nInstalling generic_bash_function in $cont..." i l
    $lxcexec bash -c "curl -s https://gitlab.com/Stylee/generic-bash-functions/-/raw/master/install.sh |bash"
    say "Done." l

    say "\nInstalling rpl in $cont..." i l
    # $lxcexec bash -c ". /opt/generic_bash_functions/functions.sh; installFromRepo https://github.com/rrthomas/rpl.git /opt/rpl/"
    pushFile "$envSetterDir/vendor/rpl-1.14/rpl" /opt/rpl/
    say "Done." l

    say "\nInstalling rpl in host..." i l
    if [[ $type == preprod || $type == prod ]]; then
	# installFromRepo https://github.com/rrthomas/rpl.git /opt/rpl/
	pushFile "$envSetterDir/vendor/rpl-1.14/rpl" /opt/rpl/ "" "host"
	aptitude install python3-chardet python3-regex
    fi
    say "Done." l

    . /opt/env_setter/env_setter-backports_repo.sh
fi

say "\nConfiguring ssh server on $cont..." l i
pushConfigFile "sshd_config" "/etc/ssh/" "$type"

f="authorized_keys"; dest="/root/.ssh/"
e="Please copy in $confFolder/$f the public key of the user who will be \
authorized to connect to the container with ssh."
pushConfigFile "$f" "$dest" "common" "$e"
$lxcexec bash -c "chmod 700 $dest; chmod 600 /root/.ssh/$f; chown root:root /root/.ssh/$f"
$lxcexec systemctl restart sshd
say "Done." l

if [ "$no_install" != "true" ] ; then
    if [[ $type == preprod || $type == prod ]]; then
	say "\nInstalling some packages on host..." l i
	apt-get install aptitude
	say "Done." l
    fi
fi
