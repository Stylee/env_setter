host=''; commandsPrefix="$lxcexec"
if [ "$1" == host ]; then
    host='host'
    commandsPrefix=""
fi
codename="$($commandsPrefix lsb_release -cs)"

repo="http://deb.debian.org/debian ${codename}-backports main"
$commandsPrefix apt-cache policy| grep -F "$repo" -q && return 0

say "\nAdding ${codename}-backports repository to the system (${commandsPrefix:+container}${host:+host})..." l i

f="/etc/apt/sources.list.d/${codename}-backports.list"

configRepo "$f" "deb $repo" "${codename}-backports" '' '' "$host"
configRepo "$f" "deb-src $repo" "${codename}-backports" '' quiet "$host"
aptUpdate
say "Done." l
