#!/bin/bash

set -e

trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT

[ $EUID -ne 0 ] && echo "You must be root to use this script." >&2 && exit 1
! command -v git &> /dev/null && echo "Please install git first." >&2 && exit 2


installDir="/opt/env_setter"
gitUrl="https://gitlab.com/Stylee/env_setter.git"


[[ -z $installDir ]] && echo "Please set up the 'installDir' variable." >&2 && exit 2

[ ! -d "$installDir" ] && mkdir "$installDir"

if [[ -z "$(ls -A $installDir)" ]]; then
    git -C "$installDir" clone "$gitUrl" .
fi

if git -C "$installDir" status &> /dev/null; then
    git -C "$installDir" pull
fi

if [[ -n "$(ls -A $installDir)" ]] && ! git -C "$installDir" status &> /dev/null
then
    echo "Not a git repo but file(s) already exists in $installDir." >&2
    echo "Check what's wrong please." >&2
    echo "Terminating" >&2 && exit 2
fi

rm -f "$installDir/.projectile"
rm -f "$installDir/todo.org"

echo -e "Installation successful. Happy hacking!\n"

