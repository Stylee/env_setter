addLineToFile() {
    local line="$1"; local file="$2"; local message="$3"; local host=$4
    [ "$host" == "host" ] && local lxcexec=""

    if ! $lxcexec [ -f "$file" ] || [ $($lxcexec grep -c "^$line$" "$file") -lt 1 ]; then
	say "$message" i
	$lxcexec bash -c "echo $line | tee -a $file"
	return 2 # this makes set -e complain.
	#https://stackoverflow.com/questions/27793264/using-set-e-set-e-in-bash-with-functions
	#hacky workaround : use `&& :` to make the function call a test
    fi
    return 0
}

commentAndOrAddKeyValue() {
    local kv="$1"; local target="$2"; local c="$3"; local k="$(cut -d'=' -f 1 <<< $kv)"

    [ -z "$c" ] && c="#"

    kEscaped=$(sed 's/[^^]/[&]/g; s/\^/\\^/g' <<<"$k")
    kvEscaped=$(sed 's/[^^]/[&]/g; s/\^/\\^/g' <<<"$kv")
    
    ! $lxcexec [ -f "$target" ] && say "File $target does not exists." >&2 && terminate 1

    $lxcexec grep -q "^[[:blank:]]*$kvEscaped$" "$target" && say "$kv is already in $target" && return 0
    
    # todo : greper avec `=` aussi. Pour pas matcher DATABASE_URL_WHATEVER pour DATABASE_URL par exemple
    # prendre en compte le cas avec et sans espace (détecter qu'elle cas s'applique en analysant $kv ?)
    # test="space = cool"; test=$(cut -f1 -d '=' <<< $test); echo "${test: -1}"
    if $lxcexec grep -q "^[[:blank:]]*$kEscaped[[:blank:]]*=" "$target"; then
	say "Key '$k' with bad value detected in $target. Commenting and adding good value."
	#https://stackoverflow.com/questions/17998763/sed-commenting-a-line-matching-a-specific-string-and-that-is-not-already-comme
	# https://stackoverflow.com/questions/29613304/is-it-possible-to-escape-regex-metacharacters-reliably-with-sed
	$lxcexec sed -i "s/^[^$c]*$kEscaped[[:blank:]]*=/$c&/" $target
    fi
    
    # https://stackoverflow.com/questions/4640011/append-text-to-file-from-command-line-without-using-io-redirection
    # https://stackoverflow.com/questions/19151954/how-to-use-variables-in-a-command-in-sed
    # It just works when I don't use any delimiter... I didn't look any further
    # Nothing is added if the file is empty ! Must have at least a blank line.
    [ $($lxcexec wc -l "$target" | cut -c1) -eq 0 ] && $lxcexec bash -c "echo '' >> \"$target\""
    $lxcexec sed -i "$ a$kv" "$target"
}

configRepo() {
    local repoFile="$1"; local repo="$2"; local repoName="$3"; local urlRepoKey="$4";
    local quiet="$5", local host="$6"
    [ "$host" == "host" ] && local lxcexec=""
    local message=""

    [ "$quiet" != quiet ] && message="Configuring $repoName repository...\n"

    addLineToFile "$repo" "$repoFile" "$message" "$host" && :
    if [ $? -eq 2 ] && [ $# -gt 3 ] && [ "$urlRepoKey" != '' ]; then
	echo "Adding $repoName signing key..."
	$lxcexec bash -c "curl -fsSL $urlRepoKey | apt-key add -"
	[ $? -ne 0 ] && echo "Signing key download failed." >&2 && terminate 1
	return 0
    fi

    return 0
}

pushConfigFile() {
    local f="$1"; local dest="$2"; local subDir="$3"; local e="$4"; local owner="$5";
    local host="$6"; local shared="$7"

    [ "$shared" == "sharedConfFile" ] && local confFolder="$envSetterDir/conf_files"

    [ "$host" == "host" ] && local lxcexec=""
    
    say "Pushing $f... " l nlb
    
    if [ ! -r "$confFolder/$subDir/$f" ] || [ ! -s "$confFolder/$subDir/$f" ]; then
	echo "$confFolder/$subDir/$f is absent, empty or non readable. Terminating..." >&2
	[ -z $e ] && exit 1
    fi

    [ -n "$e" ] && [ ! -s "$confFolder/$subDir/$f" ] && echo $e >&2 && exit 1

    pushFile "$confFolder/$subDir/$f" "$dest" "$owner" "$host"
    say "Done" l
}

pushFile () {
    local f="$1"; local dest="$2"; local owner="$3"; local host=$4
    [ "$host" == "host" ] && local lxcexec=""
    [ -z "$owner" ] && owner=0
    
    $lxcexec [ ! -d "$dest" ] && $lxcexec mkdir "$dest"
    # The destination starts with the container name, and then you
    # append the path. Note that you need to put a final / if you want
    # to put a file in a directory, else you get an error.
    if [ "$host" != "host" ]; then
	lxc file push "$f" $cont$dest --gid $owner --uid $owner
    fi

    [ "$host" == "host" ] && cp "$f" "$dest"
    return 0
}

createFolders() {
    say "\nCreating needed folders..." l i
    for f in $@; do
	echo $f && $lxcexec [ ! -d $f ] && $lxcexec mkdir $f
    done

    say "Done." l
    return 0
}

addLineBetweenComments() {
    # Beware, this function does not add the line between the comments
    # if it already exists elsewhere in the file.

    # TODO : Je ne comprends pas l'usage de sudo ici. Déjà il faudrait
    # utiliser su plutôt. A comprendre, tester, changer.
    
    local line="$1"; local file="$2"; local comment="$3"

    [ -w $file ] && local user=$(whoami) || local user="root"

    if ! grep -q "^# $comment$" "$file" ; then
	while grep -q "^# END - $comment$" "$file" ; do # No end without a start...
	    sudo sed -i "/^# END - $comment$/d" $file
	done

	echo -e "\n# $comment" | sudo -u $user tee -a $file > /dev/null
	echo -e "# END - $comment" | sudo -u $user tee -a $file > /dev/null
    fi

    local m="No end comment in $file. Please add the following line somewhere (at the beginning of its line)  :\n\
	# END - $comment\n Terminating..."
    ! grep -q "^# END - $comment$" "$file" && echo -e "$m" >&2 && exit 1

    m="Too many end comments in $file (must be only one). Do something please. Terminating..."
    [ $(grep -c "^# END - $comment$" "$file") -gt 1 ] && echo "$m" >&2 && exit 1

    local start=$(sed -n "/^# $comment$/=" $file)
    local end=$(sed -n "/^# END - $comment$/=" $file)
    m="Start and end comments are inverted. Do something Please. Terminating..."
    [ $start -gt $end ] && echo "$m" >&2 && exit 1

    if ! grep -q "^$line$" "$file"; then
	sudo sed -i "s/^# END - $comment$/$line\n# END - $comment/" $file
    fi
}

addHost() {
    local ip=$1; local hostname=$2
    m="Edited automatically by env_setter.sh."
    echo "$hostname" && addLineBetweenComments "$ip	$hostname" "/etc/hosts" "$m"
    # todo : use rpl instead ? (to remove/edit if old entry with same hostname but different ip)
}

returnDataFolder () {
    local siteDirName="$1"

    [ "$type" == "dev" ] && echo "$devDir/$siteDirName" && return
    [[ $type == preprod || $type == prod ]] && echo "/var/www/$siteDirName" && return

    say "'type' variable should be either 'dev', 'preprod' or prod'."
    terminate 1
}


sshHostCheckAdd() {
    if [[ -z "$1" ]]; then
	say "sshHostCheckAdd function must have one arguments : host to check." >&2
	say "Some variables in $deploy_conf_folder/$type/deploy-${type}.conf are probably missing." >&2
	terminate 2
    fi

    local host="$1"
    if $lxcexec su - webuser -s /bin/bash -c "ssh-keygen -H -F $host &> /dev/null"; then
	return 0;
    fi

    say "###### VERIFY ME ######" l
    $lxcexec su - webuser -s /bin/bash -c "ssh-keyscan -t rsa $host" |
	su - webuser -s /bin/bash -c "ssh-keygen -lv -f -"
    say "###### VERIFY ME ######\n" i l

    read -p "Is the fingerprint OK ? Can I add it to ~/.ssh/known_hosts? y/N ? " a
    [ "$a" != y ] && terminate 2

    if $lxcexec su - webuser -s /bin/bash -c "[ ! -d ~/.ssh ]"; then
	$lxcexec su - webuser -s /bin/bash -c "mkdir ~/.ssh"
    fi

    [ "$a" = y ] && $lxcexec su - webuser -s /bin/bash -c "ssh-keyscan -t rsa -H \"$host\" >> ~/.ssh/known_hosts"
}

cloneOrPull () {
    if [[ -z "$1" || -z "$2" || -z "$3" ]]; then
	say "cloneOrPull function must have three arguments : installation folder, repo URL and branch." >&2
	say "Some variables in $deploy_conf_folder/$type/deploy-${type}.conf are probably missing." >&2
	
	terminate 2
    fi

    say "\nCloning or updating the data from the repositories." i l

    local installDir="$1"; local gitUrl="$2"; local branch="$3"; local owner="$4"

    sshHostCheckAdd "$(echo $gitUrl |cut -d@ -f2|cut -d: -f1)"

    $lxcexec chown -R webuser:webuser $installDir

    until $lxcexec su - webuser -s /bin/bash -c "git ls-remote $gitUrl &> /dev/null"; do
	if $lxcexec su - webuser -s /bin/bash -c "[[ ! -e ~/.ssh/id_rsa.pub ]]"; then
	    say "\nGenerating a ssh key..." i l
	    $lxcexec su - webuser -s /bin/bash -c "ssh-keygen"
	    say "Done." l
	fi

	say "\n You must add the following public key in the $gitUrl repository's settings." l i
	$lxcexec su - webuser -s /bin/bash -c "cat ~/.ssh/id_rsa.pub"
	say "\nSleeping 30 seconds now." i f && sleep 30
    done
    
    $lxcexec [ ! -d  $installDir ] && $lxcexec mkdir -p "$installDir"
    local files="$($lxcexec ls -A $installDir)"
    if [ -z "$files" ]; then
	$lxcexec su - webuser -s /bin/bash -c git -C "$installDir" clone -b "$branch" --single-branch "$gitUrl" .
    fi

    if $lxcexec su - webuser -s /bin/bash -c "git -C $installDir status &> /dev/null"; then
	# Files produced by the website (images, uploaded stuff, wiki
	# data, etc) *should not be tracked* by git (and should be in
	# .gitignore), therefore should not be affected by the two
	# following commands.
	$lxcexec su - webuser -s /bin/bash -c "git -C $installDir checkout -- ."
	$lxcexec su - webuser -s /bin/bash -c "git -C $installDir clean -f -d"
	$lxcexec su - webuser -s /bin/bash -c "git -C $installDir pull"
    fi

    if [ -n "$files" ] && ! $lxcexec su - webuser -s /bin/bash -c "git -C $installDir status"
    then
	echo "Not a git repo but file(s) already exists in $installDir." >&2
	echo "Check what's wrong please." >&2
	echo "Terminating" >&2 && exit 2
    fi

    $lxcexec chown -R www-data:www-data $installDir

    [ -n "$owner" ] && $lxcexec chown -R $owner:$owner $installDir

    say "Done." l
    return 0
}
