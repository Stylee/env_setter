# todo : say what this is for

. $envSetterDir/helpers-web.sh

[ "$no_install" != "true" ] && aptUpdate

say "\nCreating .cache folder in /var/www..." l i
$lxcexec mkdir -p /var/www/.cache
$lxcexec chown -R www-data:www-data /var/www/.cache
say "Done" l

if ! $lxcexec bash -c "id webuser &>/dev/null"; then
    $lxcexec adduser webuser
    $lxcexec usermod -a -G www-data webuser
fi

function installNode { # https://github.com/tj/n
    [ "$no_install" = "true" ] && return 0

    say "\nInstalling n, then nodejs with n (on ${1:-' container'})..." l i
    [ "$1" == host ] && local lxcexec=""
    local tmpfile=/tmp/n-installer-$(date +%s)
    $lxcexec curl -L https://raw.githubusercontent.com/tj/n/master/bin/n -o $tmpfile
    $lxcexec bash $tmpfile lts
    $lxcexec rm $tmpfile
    say "Done.\n" l i
}

[[ $type == preprod || $type == prod ]] && installNode
[[ $type == dev ]] && installNode host

if [ "$no_install" != "true" ] ; then
    . /opt/env_setter/env_setter-nginx.sh

    say "\nInstalling some packages..." l i
    $lxcexec aptitude install php-xml php-cgi php-fpm php-curl php-zip php-intl\
	     php-mbstring php-mysql apache2-utils php-gd php-imagick
    $lxcexec aptitude install -t $($lxcexec lsb_release -cs)-backports
    if [[ $type == "dev" || -z $type ]] && [[ $DB_needed != "no" ]]; then
	$lxcexec aptitude install adminer
    fi
    [[ $DB_needed != "no" ]] && $lxcexec aptitude install mariadb-server

    . $envSetterDir/install_composer.sh

    say "Done." l
fi

say "\n Configuring php..." l i
if [ -e "$envSetterDir/conf_files/${cont//-$type/}/php/fpm/${type}/php.ini" ]; then
    $lxcexec mkdir -p /var/log/php
    $lxcexec chown -R www-data:www-data /var/log/php
    pushConfigFile "php.ini" "/etc/php/$(echoPhpV)/fpm/" "${cont//-$type/}/php/fpm/${type}/" '' '' '' 'sharedConfFile'
else
    say "INFO: No php.ini in $envSetterDir/conf_files/${cont//-$type/}/php/fpm/${type}/ ."
fi
say "Done" l
