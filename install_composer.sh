#!/bin/sh

say "\nInstalling composer if not present..." l i

if ! $lxcexec bash -c "command -v composer &> /dev/null" ; then
    EXPECTED_CHECKSUM="$(wget -q -O - https://composer.github.io/installer.sig)"
    $lxcexec php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    ACTUAL_CHECKSUM="$($lxcexec php -r "echo hash_file('sha384', 'composer-setup.php');")"

    if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]; then
	>&2 echo 'ERROR: Invalid installer checksum'
	$lxcexec rm composer-setup.php
	exit 1
    fi

    $lxcexec php composer-setup.php --quiet
    RESULT=$?
    $lxcexec rm composer-setup.php
    $lxcexec mv composer.phar /usr/local/bin/composer

    [ $RESULT -ne 0 ] && echo "Something went wrong. Are you connected to the internet ?" >&2
    return $RESULT
fi

say "Done." l
